<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>CESUR Desarrollo Web Entorno Servidor</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Formularios. Creación</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav me-auto mb-2 mb-md-0">
      </ul>
    </div>
  </div>
</nav>
<div class="container">
  <div class="row">
    <div class="col">
      <form action="/formulario.php" method="post">
        <div class="mb-3">
          <label for="calle" class="form-label">Calle</label>
          <input type="text" class="form-control" name="calle" id="calle">
        </div>
        <div class="mb-3">
          <label for="numero" class="form-label">Número</label>
          <input type="text" class="form-control" name="numero" id="numero">
        </div>
        <div class="mb-3">
          <label for="poblacion" class="form-label">Población</label>
          <input type="text" class="form-control" name="poblacion" id="poblacion">
        </div>
        <div class="mb-3">
          <label for="pais" class="form-label">País</label>
          <input type="text" class="form-control" name="pais" id="pais">
        </div>
        <button type="submit" class="btn btn-primary">Enviar</button>
      </form>
    </div>
  </div>
</div>
</body>
</html>
